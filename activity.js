// Item 2
db.fruits.aggregate([{ $match: { onSale: true } }, { $count: "fruitsOnSale" }]);

// Item 3
db.fruits.aggregate([
  { $match: { stock: { $gt: 20 } } },
  { $count: "enoughStock" },
]);

// Item 4
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplierId", avgPrice: { $avg: "$price" } } },
  { $sort: { avgPrice: 1 } },
]);

// Item 5
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplierId", maxPrice: { $max: "$price" } } },
  { $sort: { maxPrice: 1 } },
]);

// Item 6
db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplierId", minPrice: { $min: "$price" } } },
  { $sort: { minPrice: 1 } },
]);
